package pl.szymanski.todo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.szymanski.todo.enums.Priority;
import pl.szymanski.todo.enums.SortCategory;
import pl.szymanski.todo.enums.SortDirection;
import pl.szymanski.todo.schema.TaskContract;
import pl.szymanski.todo.schema.TaskModel;
import pl.szymanski.todo.utils.PriorityUtils;
import pl.szymanski.todo.utils.SortingUtils;

public class TaskDao {

    private static final String[] projection = {
            BaseColumns._ID,
            TaskContract.TaskEntry.COLUMN_NAME_TITLE,
            TaskContract.TaskEntry.COLUMN_NAME_DONE,
            TaskContract.TaskEntry.COLUMN_NAME_PRIORITY,
            TaskContract.TaskEntry.COLUMN_NAME_END_DATE,
            TaskContract.TaskEntry.COLUMN_NAME_PLANNED_END_DATE,
            TaskContract.TaskEntry.COLUMN_NAME_CREATED_DATE
    };

    public static final long createTask(final String taskName, final Context context) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_NAME_TITLE, taskName);
        values.put(TaskContract.TaskEntry.COLUMN_NAME_CREATED_DATE, System.currentTimeMillis());

        return db.insert(TaskContract.TaskEntry.TABLE_NAME, null, values);
    }

    public static final List<TaskModel> getAllEntries(final Context context, SortCategory sortCategory, SortDirection sortDirection) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String orderBy = null;
        if (sortCategory != null && sortDirection != null) {
            orderBy = SortingUtils.resolveTaskField(sortCategory) + " " + sortDirection;
        }

        Cursor cursor = db.query(
                TaskContract.TaskEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                orderBy
        );

        final List<TaskModel> tasks = new ArrayList<>();
        while (cursor.moveToNext()) {
            TaskModel task = convertCursorToTask(cursor);
            tasks.add(task);
        }

        return tasks;
    }

    private static TaskModel convertCursorToTask(final Cursor cursor) {
        final TaskModel task = new TaskModel();
        task.setId(cursor.getLong(cursor.getColumnIndex(TaskContract.TaskEntry._ID)));
        task.setTitle(cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_TITLE)));
        task.setDone(cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DONE)) == 1);
        task.setPriority(PriorityUtils.resolvePriority(cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_PRIORITY))));
        long createdDate = cursor.getLong(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_CREATED_DATE));
        task.setCreatedDate(new Date(createdDate));
        long endDate = cursor.getLong(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_END_DATE));
        task.setEndTime(new Date(endDate));
        long plannedEndDate = cursor.getLong(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_PLANNED_END_DATE));
        if (plannedEndDate > 0) {
            task.setPlannedEndTime(new Date(plannedEndDate));
        }
        return task;
    }

    public static final void deleteTask(final long taskId, final Context context) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selection = TaskContract.TaskEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(taskId)};
        db.delete(TaskContract.TaskEntry.TABLE_NAME, selection, selectionArgs);
    }

    public static final TaskModel getTaskById(final long taskId, final Context context) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selection = TaskContract.TaskEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(taskId)};
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, null);
        if (cursor.getCount() > 1) {
            throw new IllegalStateException("It wasn`t unique search for task");
        }

        cursor.moveToNext();
        return convertCursorToTask(cursor);
    }

    public static final void updateTaskTitle(final long taskId, final String newTitle, final Context context) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);
        final ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_NAME_TITLE, newTitle);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selection = TaskContract.TaskEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(taskId)};

        db.update(TaskContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    public static final void updateTaskDoneColumn(final long taskId, final boolean isDone, final Context context) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);
        final ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_NAME_DONE, isDone ? 1 : 0);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selection = TaskContract.TaskEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(taskId)};
        if (isDone) {
            values.put(TaskContract.TaskEntry.COLUMN_NAME_END_DATE, System.currentTimeMillis());
        }

        db.update(TaskContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    public static final void updateTaskPriority(final long taskId, final Priority priority, final Context context) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);
        final ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_NAME_PRIORITY, PriorityUtils.resolvePriorityCode(priority));
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selection = TaskContract.TaskEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(taskId)};

        db.update(TaskContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    public static final void setTaskPlannedEndDate(final long taskId, final Date plannedEndDate, final Context context) {
        final TaskContract.TaskEntry.TaskReaderDbHelper dbHelper = new TaskContract.TaskEntry.TaskReaderDbHelper(context);
        final ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_NAME_PLANNED_END_DATE, plannedEndDate.getTime());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selection = TaskContract.TaskEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(taskId)};

        db.update(TaskContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);
    }
}
