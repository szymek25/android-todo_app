package pl.szymanski.todo.enums;

public enum Priority {

    LOW,
    MEDIUM,
    HIGH
}
