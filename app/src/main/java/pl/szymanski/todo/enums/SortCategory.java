package pl.szymanski.todo.enums;

public enum SortCategory {
    PRIORITY,
    NAME,
    STATUS
}
