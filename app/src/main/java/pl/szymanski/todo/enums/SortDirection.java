package pl.szymanski.todo.enums;

public enum SortDirection {
    ASC,
    DESC
}
