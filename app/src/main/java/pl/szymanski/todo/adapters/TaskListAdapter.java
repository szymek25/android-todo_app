package pl.szymanski.todo.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import pl.szymanski.todo.R;
import pl.szymanski.todo.TaskDetails;
import pl.szymanski.todo.dao.TaskDao;
import pl.szymanski.todo.enums.Priority;
import pl.szymanski.todo.schema.TaskModel;

public class TaskListAdapter extends BaseAdapter implements ListAdapter {
    private List<TaskModel> list;
    private Context context;


    public TaskListAdapter(List<TaskModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void add(final TaskModel task) {
        list.add(task);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return list.get(pos).getId();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.task_list, null);
        }

        TextView listItemText = view.findViewById(R.id.list_item_string);
        final TaskModel taskModel = list.get(position);
        listItemText.setText(taskModel.getTitle());

        final CheckBox taskDoneCheckbox = view.findViewById(R.id.doneCheckbox);
        taskDoneCheckbox.setChecked(taskModel.isDone());

        final ImageView priorityIcon = view.findViewById(R.id.priority_icon);
        if(taskModel.getPriority()!= null) {
            final Priority priority = taskModel.getPriority();
            switch (priority) {
                case LOW:
                    priorityIcon.setImageResource(R.drawable.low);
                    break;
                case MEDIUM:
                    priorityIcon.setImageResource(R.drawable.medium);
                    break;
                case HIGH:
                    priorityIcon.setImageResource(R.drawable.high);
                    break;
            }
        } else {
            priorityIcon.setImageResource(R.drawable.low);
        }

        Button deleteBtn = view.findViewById(R.id.delete_btn);
        Button editBtn = view.findViewById(R.id.edit_btn);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskDao.deleteTask(getItemId(position), v.getContext());
                list.remove(position);
                notifyDataSetChanged();
            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent taskDetailIntent = new Intent(v.getContext(), TaskDetails.class);
                Bundle b = new Bundle();
                b.putLong(v.getContext().getString(R.string.task_id), getItemId(position));
                taskDetailIntent.putExtras(b);
                v.getContext().startActivity(taskDetailIntent);
            }
        });

        taskDoneCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskDao.updateTaskDoneColumn(getItemId(position), ((CheckBox) v).isChecked(), v.getContext());
            }
        });

        return view;
    }


}
