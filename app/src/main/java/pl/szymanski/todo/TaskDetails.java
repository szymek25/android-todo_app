package pl.szymanski.todo;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import pl.szymanski.todo.dao.TaskDao;
import pl.szymanski.todo.schema.TaskModel;
import pl.szymanski.todo.utils.PriorityUtils;

public class TaskDetails extends AppCompatActivity {

    private EditText taskTitle;
    private Button confButton;
    private Spinner prioritySelector;
    private TextView mDisplayCreationDate;
    private TextView mDisplayPlannedDate;
    private DatePickerDialog.OnDateSetListener mPlannedDateSetListener;
    private TextView mEndDateLabel;
    private TextView mDisplayEndDate;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        taskTitle = findViewById(R.id.tastTitle);
        confButton = findViewById(R.id.confButton);

        Bundle bundle = getIntent().getExtras();
        final long taskId = bundle.getLong(getString(R.string.task_id));
        TaskModel task = TaskDao.getTaskById(taskId, this);
        taskTitle.setText(task.getTitle());

        prioritySelector = findViewById(R.id.prioritySelector);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.priorities, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySelector.setAdapter(adapter);

        confButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newTitle = taskTitle.getText().toString();
                TaskDao.updateTaskTitle(taskId, newTitle, getApplicationContext());
                finish();
            }
        });

        prioritySelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TaskDao.updateTaskPriority(taskId, PriorityUtils.resolvePriority(position), view.getContext());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mDisplayCreationDate = findViewById(R.id.mDisplayCreationDate);
        Date createdDate = task.getCreatedDate();
        if (createdDate != null) {
            mDisplayCreationDate.setText(sdf.format(createdDate));
        }

        mDisplayPlannedDate = findViewById(R.id.mDisplayPlannedDate);
        final Date plannedEndTime = task.getPlannedEndTime();
        if (plannedEndTime != null) {
            mDisplayPlannedDate.setText(sdf.format(plannedEndTime));
        }

        mDisplayPlannedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                if (plannedEndTime != null) {
                    cal.setTime(plannedEndTime);
                }
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        TaskDetails.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mPlannedDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mPlannedDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;

                String date = day + "/" + month + "/" + year;
                try {
                    Date selectedPlannedDate = sdf.parse(date);
                    TaskDao.setTaskPlannedEndDate(taskId, selectedPlannedDate, view.getContext());
                    mDisplayPlannedDate.setText(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };

        Date endTime = task.getEndTime();
        mEndDateLabel = findViewById(R.id.endDateLabel);
        mDisplayEndDate = findViewById(R.id.mDisplayEndDate);
        if (task.isDone()) {
            mEndDateLabel.setVisibility(View.VISIBLE);
            mDisplayEndDate.setText(sdf.format(endTime));
        }

    }
}
