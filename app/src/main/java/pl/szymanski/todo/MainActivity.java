package pl.szymanski.todo;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pl.szymanski.todo.adapters.TaskListAdapter;
import pl.szymanski.todo.dao.TaskDao;
import pl.szymanski.todo.enums.SortCategory;
import pl.szymanski.todo.enums.SortDirection;
import pl.szymanski.todo.schema.TaskModel;
import pl.szymanski.todo.utils.PriorityUtils;
import pl.szymanski.todo.utils.SortingUtils;

public class MainActivity extends AppCompatActivity {

    private Button createTodoItemButton;
    private EditText entryName;
    private ListView listView;
    private Spinner sortCategorySpinner;
    private Spinner sortDirectionSpinner;
    private SortCategory selectedSortCategory;
    private SortDirection selectedSortDirection = SortDirection.ASC;
    private Button exportButton;
    private SimpleDateFormat datetFormatForFileName = new SimpleDateFormat("dd_MM_yyyy_kk:mm:ss:SSS");
    private SimpleDateFormat dateFormatForCreatedDate = new SimpleDateFormat("dd/MM/yyyy");

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createTodoItemButton = findViewById(R.id.createTodoEntry);
        entryName = findViewById(R.id.newEntryName);
        listView = findViewById(R.id.todoEntries);

        final TaskListAdapter taskListAdapter = createTaskListAdapter();
        listView.setAdapter(taskListAdapter);


        createTodoItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = entryName.getText().toString();
                if (value.isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Wprowadź nazwę zadania", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    long insertId = TaskDao.createTask(value, v.getContext());

                    entryName.setText("");
                    entryName.clearFocus();
                    ListView taskListView = findViewById(R.id.todoEntries);
                    TaskListAdapter taskListViewAdapter = (TaskListAdapter) taskListView.getAdapter();
                    taskListViewAdapter.add(new TaskModel(insertId, value));
                }
            }
        });

        sortCategorySpinner = findViewById(R.id.sortCategory);
        ArrayAdapter<CharSequence> sortCategoryAdapter = ArrayAdapter.createFromResource(this, R.array.sortCategories, android.R.layout.simple_spinner_item);
        sortCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortCategorySpinner.setAdapter(sortCategoryAdapter);

        sortCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SortCategory sortCategory = SortingUtils.resolveSortCategoryEnum(position);
                selectedSortCategory = sortCategory;
                TaskListAdapter updatedTaskListAdapter = createTaskListAdapter();
                listView.setAdapter(updatedTaskListAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sortDirectionSpinner = findViewById(R.id.sortDirection);
        ArrayAdapter<CharSequence> sortDirectionAdapter = ArrayAdapter.createFromResource(this, R.array.sortDirection, android.R.layout.simple_spinner_item);
        sortDirectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortDirectionSpinner.setAdapter(sortDirectionAdapter);

        sortDirectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SortDirection sortDirection = SortingUtils.resolveSortDirectionEnum(position);
                selectedSortDirection = sortDirection;
                if (selectedSortCategory != null) {
                    TaskListAdapter updatedTaskListAdapter = createTaskListAdapter();
                    listView.setAdapter(updatedTaskListAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        exportButton = findViewById(R.id.exportButton);
        exportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportTasksToFile(v.getContext());
            }
        });
    }

    private TaskListAdapter createTaskListAdapter() {
        List<TaskModel> allEntries = TaskDao.getAllEntries(getApplicationContext(), selectedSortCategory, selectedSortDirection);

        return new TaskListAdapter(allEntries, this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        TaskListAdapter taskListAdapter = createTaskListAdapter();
        listView.setAdapter(taskListAdapter);
    }

    private void exportTasksToFile(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        try {
            File root = new File(Environment.getExternalStorageDirectory(),"Tasks");
            if (!root.exists()) {
                root.mkdirs();
            }
            final String sFileName = "tasks_" + datetFormatForFileName.format(new Date());
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(createTasksFileBody(context));
            writer.flush();
            writer.close();
            Toast.makeText(context, "Utworzono plik: " + sFileName, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String createTasksFileBody(final Context context) {
        final StringBuilder body = new StringBuilder();
        String[] priorites = getResources().getStringArray(R.array.priorities);
        List<TaskModel> allEntries = TaskDao.getAllEntries(context, null, null);
        for (final TaskModel task : allEntries) {
            body.append("Tytuł: " + task.getTitle());
            body.append(" ");
            body.append("Priorytet: " + priorites[PriorityUtils.resolvePriorityCode(task.getPriority())]);
            body.append(" ");
            body.append("Utworzono: " + dateFormatForCreatedDate.format(task.getCreatedDate()));
            body.append("\n");
        }

        return body.toString();
    }
}
