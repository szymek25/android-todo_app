package pl.szymanski.todo.utils;

import java.util.HashMap;
import java.util.Map;

import pl.szymanski.todo.enums.SortCategory;
import pl.szymanski.todo.enums.SortDirection;
import pl.szymanski.todo.schema.TaskContract;

public class SortingUtils {

    private static final Map<SortCategory, String> mapSortCategory2TaskModelField = new HashMap<SortCategory, String>() {{
        put(SortCategory.PRIORITY, TaskContract.TaskEntry.COLUMN_NAME_PRIORITY);
        put(SortCategory.NAME, TaskContract.TaskEntry.COLUMN_NAME_TITLE);
        put(SortCategory.STATUS, TaskContract.TaskEntry.COLUMN_NAME_DONE);
    }};

    public static final SortCategory resolveSortCategoryEnum(final int sortCategoryCode) {
        SortCategory resolvedSortCategory = null;
        switch (sortCategoryCode) {
            case 1:
                resolvedSortCategory = SortCategory.PRIORITY;
                break;
            case 2:
                resolvedSortCategory = SortCategory.NAME;
                break;
            case 3:
                resolvedSortCategory = SortCategory.STATUS;
                break;
        }

        return resolvedSortCategory;
    }

    public static final SortDirection resolveSortDirectionEnum(final int sortDirectionCode) {
        SortDirection resolvedSortDirection = null;
        switch (sortDirectionCode) {
            case 0:
                resolvedSortDirection = SortDirection.ASC;
                break;
            case 1:
                resolvedSortDirection = SortDirection.DESC;
                break;
        }

        return resolvedSortDirection;
    }

    public static final String resolveTaskField(final SortCategory sortCategory) {
        return mapSortCategory2TaskModelField.get(sortCategory);
    }
}
