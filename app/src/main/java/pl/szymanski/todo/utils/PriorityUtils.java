package pl.szymanski.todo.utils;

import pl.szymanski.todo.enums.Priority;

public class PriorityUtils {

    private static final int unresolvedPriorityCode = -99999;

    public static final Priority resolvePriority(final int priorityCode) {
        Priority resolvedPriority = null;
        switch (priorityCode) {
            case 0:
                resolvedPriority = Priority.LOW;
                break;
            case 1:
                resolvedPriority = Priority.MEDIUM;
                break;
            case 2:
                resolvedPriority = Priority.HIGH;
                break;
        }


        return resolvedPriority;
    }

    public static final int resolvePriorityCode(final Priority priority) {
        int priorityCode = unresolvedPriorityCode;
        switch (priority) {
            case LOW:
                priorityCode = 0;
                break;
            case MEDIUM:
                priorityCode = 1;
                break;
            case HIGH:
                priorityCode = 2;
                break;
        }

        return priorityCode;

    }
}
