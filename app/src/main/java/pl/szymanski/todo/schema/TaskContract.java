package pl.szymanski.todo.schema;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class TaskContract {

    private TaskContract() {
    }

    public static class TaskEntry implements BaseColumns {
        public static final String TABLE_NAME = "task";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_DONE = "done";
        public static final String COLUMN_NAME_PRIORITY = "priority";
        public static final String COLUMN_NAME_CREATED_DATE = "createdDate";
        public static final String COLUMN_NAME_PLANNED_END_DATE = "plannedEndDate";
        public static final String COLUMN_NAME_END_DATE = "endDate";

        private static final String SQL_CREATE_TASKS =
                "CREATE TABLE " + TaskEntry.TABLE_NAME + " (" +
                        TaskEntry._ID + " INTEGER PRIMARY KEY," +
                        TaskEntry.COLUMN_NAME_TITLE + " TEXT," +
                        TaskEntry.COLUMN_NAME_PRIORITY + " INTEGER DEFAULT 0," +
                        TaskEntry.COLUMN_NAME_DONE + " INTEGER DEFAULT 0," +
                        TaskEntry.COLUMN_NAME_CREATED_DATE + " INTEGER," +
                        TaskEntry.COLUMN_NAME_PLANNED_END_DATE + " INTEGER," +
                        TaskEntry.COLUMN_NAME_END_DATE + " INTEGER" +
                        ")";

        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TaskEntry.TABLE_NAME;

        public static class TaskReaderDbHelper extends SQLiteOpenHelper {
            public static final int DATABASE_VERSION = 1;
            public static final String DATABASE_NAME = "Todo.db";

            public TaskReaderDbHelper(Context context) {
                super(context, DATABASE_NAME, null, DATABASE_VERSION);
            }

            public void onCreate(SQLiteDatabase db) {
                db.execSQL(SQL_CREATE_TASKS);
            }

            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                db.execSQL(SQL_DELETE_ENTRIES);
                onCreate(db);
            }

            public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                onUpgrade(db, oldVersion, newVersion);
            }
        }

    }
}
